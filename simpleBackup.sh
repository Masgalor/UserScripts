#!/bin/bash

########################################################################
################### Script to create offsite backups ###################
########################################################################


########################################################################
######## This script archives all production data including the
######## MariaDB databases and makes them available for download.
########
######## Recommended alias:
######## alias backup='/opt/Scripts/simpleBackup.sh'

########################################################################
######## Define the internal folder structure.
BACKUP_DIR="$( mktemp -d )"
SOURCE="/srv"

########################################################################
######## Read live values from the system.
DATE="$( date +%Y%m%d )"
HOSTNAME="$( hostname --fqdn )"
SIZE_SOURCE="$( sudo du -s "$SOURCE" | awk 'NR==1 {print $1}' )"
SIZE_DESTINATION="$( sudo df -P "$BACKUP_DIR" | awk 'NR==2 {print $4}' )"

########################################################################
######## Check if the basic requirements are met.
[ ! -d "$BACKUP_DIR" ] && echo "Creation of temporary directory failed." && exit
[ "$SIZE_SOURCE" -gt "$SIZE_DESTINATION" ] && echo "There is not enough space available to create the backup." && exit

########################################################################
######## Create a temporary folder and dump all archived data into it.
sudo tar -cvzpf $BACKUP_DIR/$HOSTNAME-$DATE.tar.gz $SOURCE
command -v mariabackup >/dev/null 2>&1 && sudo mariabackup --user=root --backup --stream=xbstream | gzip > $BACKUP_DIR/$HOSTNAME-$DATE-Database.gz

########################################################################
######## Start a temporary http server to download the files.
cd $BACKUP_DIR
python3 -m http.server --bind 127.0.0.1 8088

########################################################################
######## Clean up all temporary data.
sudo rm -rf $BACKUP_DIR >/dev/null 2>&1
