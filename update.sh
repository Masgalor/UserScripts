#!/bin/bash

########################################################################
############################# Updatescript #############################
########################################################################


########################################################################
######## This script performs package-updates.
######## Basicly it's the same as "apt update && apt upgrade"
########
######## Recommended alias:
######## alias update='/opt/Scripts/update.sh -em full'

########################################################################
######## Define packages that should never be installed.
_IgnoredPackages=""
########################################################################


########################################################################
########  Check if the script was executed by root
########  If it's a normal user activate "sudo" where neccessary
SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi
########################################################################

########################################################################
########  Check if additional parameters where supplied
########  Set variables to store valid options
DeepClean=false
AutoDownload=false
stdoutHandling='1>/dev/null'
stderrHandling='2>/dev/null'

#Get Arguments
while getopts ":adem:o" opt; do
  case $opt in
    a) AutoDownload=true
    ;;
    d) DeepClean=true
    ;;
    e) stderrHandling=''
    ;;
    m) Mode="$OPTARG"
    ;;
    o) stdoutHandling=''
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done
########################################################################

########################################################################
######## Reformat the user defined list of ignored packages
######## Packages that are not present in apt-cache are ignored
IgnoredPackages=""
for package in $_IgnoredPackages; do
    if apt-cache show "$package" 1>/dev/null 2>&1; then
        IgnoredPackages+="$package- "
    fi
done
########################################################################

########################################################################
######## Definition of all modules

######## Clean
######## Clean apt cache an uninstall unused packages
_clean(){
  echo -e "*** Cleaning up *** \n"
  if [ "$DeepClean" = true ]; then
    echo 'Cleaning package-repository cache.' 2>/dev/null
    eval "$SUDO apt-get clean -qy $stdoutHandling $stderrHandling"
    eval "$SUDO apt-get autoclean -qy $stdoutHandling $stderrHandling"
  fi
    echo 'Removing unused packages.' 2>/dev/null
    eval "$SUDO apt-get autoremove -qy $stdoutHandling $stderrHandling"
}

######## Download
######## Download new package lists and upgradeable packages
_download(){
  echo -e "\n*** Downloading update information ***\n"
  echo 'Rebuilding package-repository cache.' 2>/dev/null
  eval "$SUDO apt-get update -qy $stdoutHandling $stderrHandling"
  if [ "$AutoDownload" = true ]; then
      echo 'Downloading upgradeable packages.' 2>/dev/null
      eval "$SUDO apt-get upgrade $IgnoredPackages -qdy $stdoutHandling $stderrHandling"
  fi
  if [ -x "/usr/bin/cscli" ]; then
    echo 'Refreshing crowdsec component information.' 2>/dev/null
    eval "$SUDO cscli hub update --error $stdoutHandling $stderrHandling"
  fi
}

######## Install
######## Install upgradeable packages
_install(){
  echo -e "\n*** Installing updates ***\n"
  echo 'Upgrading packages from apt sources.' 2>/dev/null
  export NEEDRESTART_SUSPEND="y"
  eval "$([ ! -z $SUDO ] && echo '$SUDO NEEDRESTART_SUSPEND=y') apt-get dist-upgrade $IgnoredPackages -q$([ ! -z $stdoutHandling ] && echo 'y') $stdoutHandling $stderrHandling"
  if [ -x "/usr/bin/cscli" ]; then
    echo 'Upgrading crowdsec components.' 2>/dev/null
    eval "$SUDO cscli hub upgrade --force --error $stdoutHandling $stderrHandling"
  fi
  if [ -x "/usr/bin/podman" ]; then
    echo 'Upgrading podman containers.' 2>/dev/null
    eval "$SUDO podman auto-update $stdoutHandling $stderrHandling"
  fi
}

######## Announce
######## Announce changes to security software like rkhunter
_announce(){
  echo -e "\n*** Running post update tasks ***\n"
  if [ -x "/usr/bin/rkhunter" ]; then
    echo 'Refreshing file propperties with rkhunter' 2>/dev/null
    eval "$SUDO rkhunter --propupd $stdoutHandling $stderrHandling"
  fi
  if [ -x "/usr/sbin/needrestart" ]; then
    eval "$SUDO needrestart -qr i"
  fi
}
########################################################################

########################################################################
######## Call the requested functions
clear
case "$Mode" in
  clean)
    _clean
  ;;

  download)
    _download
  ;;

  install)
    _install
  ;;

  announce)
    _announce
  ;;

  full)
    _clean
    sleep 1
    _download
    sleep 1
    _install
    sleep 1
    _announce
  ;;

  finish)
    _install
    sleep 1
    _announce
  ;;

  *)
    echo "Usage $0 -m {clean|download|install|announce|full|finish}"
    echo "Options:"
    echo "-a        AutoDownload, automatically download new packages"
    echo "-d        DeepClean, clear apt cache before updating"
    echo "-e        Print errors, enables error messages"
    echo "-m        Mode, specifies what actions to perform"
    echo "-o        Print standard output, shows the output of performed commands"
  ;;
esac
########################################################################
