# These Scripts are meant to simplify or improve tasks for users.

It's recommendet to install them to "/opt/Scripts".

Every file has a description in it, there is also described what aliases are recommendet for best user experience.

### This code snippet is used to install the scripts.
```
#!/bin/bash
git clone "https://codeberg.org/Masgalor/UserScripts.git" "/opt/Scripts"
```
### This code snippet is used to update the scripts.
```
#!/bin/bash
git -C "/opt/Scripts" pull
```
### This code snippet can be used to set the recommendet permissions for all installed scripts.
```
#!/bin/bash
chmod 555 /opt/Scripts/*.sh
```
