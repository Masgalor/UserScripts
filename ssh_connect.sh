#!/bin/bash

########################################################################
############## SSH connect script with smartcard support ###############
########################################################################


########################################################################
######## This script establishes a connection to a remote ssh server.
######## It supports smartcards if the "opensc-tool" is installed.
########
######## The following parameters can be set:
########   1) Host
########   2) Port
########   3) User
########   4) Tunnel Request (can consist out of a single port number or a remote and a local port separated by a colon)
########
######## The following options can be set:
########  -v) Verbose
########  -o) Use OpenSc instead of the global key sources
########  -h) Use the hostname as remote target instead of localhost when tunnelling
########
######## Recommended alias:
######## alias connect='/opt/Scripts/ssh_connect.sh'
########################################################################

########################################################################
######## Specify the pkcs11 library
LIB_Opensc="/usr/lib/x86_64-linux-gnu/pkcs11/opensc-pkcs11.so"
########################################################################

########################################################################
########  Check if additional parameters were supplied
########  Set variables to store valid options
Verbose=false
OpenSc=false
UseHostname=false

for option in "$@"; do
  case "$option" in
    -v | -V)
      Verbose=true
      ;;
    -o | -O)
      OpenSc=true
     ;;
    -h | -H)
      UseHostname=true
      ;;
    *)
      [ -z "$Host" ] && Host="$option" && continue
      [ -z "$Port" ] && Port="$option" && continue
      [ -z "$User" ] && User="$option" && continue
      [ -z "$Tunnel_Request" ] && Tunnel_Request="$option"
    ;;
  esac
done
########################################################################

########################################################################
######## Build a string containing the connection parameters
[ -z "$Host" ] && echo "Hostname is empty." && exit
host_string="$Host"
[ -z "$Port" ] || host_string+=" -p $Port"
[ -z "$User" ] || host_string="$User@$host_string"
if [ "$Verbose" = true ]; then
  echo "Connection details:"
  echo "Host: $Host"
  echo "Port: $Port"
  echo "User: $User"
  echo "Complete string: $host_string"
  echo ""
fi
########################################################################

########################################################################
######## Check if a smartcard is accessable and enable it
cardlist=""
cardlist=$(opensc-tool --list-readers | grep "Yes")
if [ "$cardlist" != "" ] && [ "$OpenSc" = true ] && [ -e "$LIB_Opensc" ]; then
    command_string="ssh -I $LIB_Opensc $host_string"
else
    command_string="ssh $host_string"
fi
if [ "$Verbose" = true ]; then
    echo "Available smartcards:"
    echo "$cardlist"
    echo ""
    if [ "$OpenSc" = true ]; then
        echo "OpenSc is selected."
        echo "The following library will be used."
        echo "$LIB_Opensc"
    fi
    echo ""
fi
########################################################################

########################################################################
######## Check if a tunnel is requested and enable it
if [ ! -z "$Tunnel_Request" ]; then
  if [[ "$Tunnel_Request" =~ ^[0-9]{1,5}+:[0-9]{1,5}$ ]]; then
    IFS=':' read -ra Tunnel_Portlist <<< "$Tunnel_Request"
    Tunnel_Port_Remote="${Tunnel_Portlist[0]}"
    Tunnel_Port_Local="${Tunnel_Portlist[1]}"
  elif [[ "$Tunnel_Request" =~ ^[0-9]{1,5}$ ]]; then
    Tunnel_Port_Remote="$Tunnel_Request"
    Tunnel_Port_Local="$Tunnel_Request"
  fi
fi
if [ ! -z "$Tunnel_Port_Remote" ] && [ ! -z "$Tunnel_Port_Local" ]; then
  if [ "$UseHostname" = true ]; then
    command_string+=" -L $Tunnel_Port_Local:$Host:$Tunnel_Port_Remote"
  else
    command_string+=" -L $Tunnel_Port_Local:127.0.0.1:$Tunnel_Port_Remote"
  fi
fi
if [ "$Verbose" = true ]; then
  if [ "$UseHostname" = true ]; then
    echo "UseHostname is selected."
    echo "Tunnel is established with $Host instead of localhost."
  fi
  echo "Final command:"
  echo "$command_string"
  echo ""
fi
########################################################################

########################################################################
######## Run the final command
eval "$command_string"
########################################################################
