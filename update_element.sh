#!/bin/bash

########################################################################
############ Script to install the latest Element version ##############
########################################################################


########################################################################
######## This script installs the newest available version.
########
######## Recommended alias:
######## alias update_element='/opt/Scripts/update_element.sh'
######## alias update_riot='/opt/Scripts/update_element.sh'

########################################################################
######## Get the latest release via Github api.
raw_release=$(curl -s https://api.github.com/repos/vector-im/element-web/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 2-)
echo "Element v$raw_release will be installed"

########################################################################
######## Download the latest files to a temporary location.
mkdir -p /tmp/element-web/src 2>/dev/null
wget --inet4-only --show-progress -qO "/tmp/element-web/element-web.tar.gz" "https://github.com/vector-im/element-web/releases/download/v$raw_release/element-v$raw_release.tar.gz"
wget --inet4-only --show-progress -qO "/tmp/element-web/element-web.tar.gz.asc" "https://github.com/vector-im/element-web/releases/download/v$raw_release/element-v$raw_release.tar.gz.asc"

########################################################################
######## Verify the download and install the new version.
if ( gpg --verify /tmp/element-web/element-web.tar.gz.asc /tmp/element-web/element-web.tar.gz 2>/dev/null ); then
  # Exchange Data
  tar -xzf /tmp/element-web/element-web.tar.gz -C /tmp/element-web/src/ 2>/dev/null
  sudo chown -R www-data:www-data /tmp/element-web/src
  sudo cp -r /tmp/element-web/src/*/* /srv/Nutzdaten/Websites/Element/
  echo "Installation finished"
else
  echo "Download is invalid, terminating..."
fi

########################################################################
######## Clean up all temporary data.
sudo rm -rf /tmp/element-web
