#!/bin/bash

########################################################################
#################### Script to sync TLS certificates ###################
########################################################################


########################################################################
######## This script pulls TLS certificates from another server.
######## It synchronizes the whole structure "/srv/TLSCerts".
######## Identities and CSR-requests are ignored.
######## And reloads/restarts all configured services thereafter.
########
######## Recommended alias:
######## alias refreshcerts='/opt/Scripts/refreshCerts.sh'

########################################################################
######## Define the server to syncronize with.
SOURCE_SERVER="base02.masgalor"

########################################################################
######## Define what services to reload or restart.
SERVICE_LIST_RELOAD="nginx postfix ejabberd"
SERVICE_LIST_RESTART="coturn matrix-synapse"

########################################################################
######## Synchronize the folder-structure holding the certificates.
######## Skip this step if the script is running on the target server.
if ! [[ $(hostname --fqdn) =~ "$SOURCE_SERVER" ]]; then
  sudo rsync -avzh --delete --chown=root:root --exclude=*.key --exclude=/CSR/ rsync://"$SOURCE_SERVER"/TLSCerts/ /srv/TLSCerts/
fi

########################################################################
######## Reload all configured services if they are available.
for service in $SERVICE_LIST_RELOAD; do
  systemctl list-units --full -all | grep -Fq "$service.service" && echo "Reloading $service .."  && sudo service "$service" reload
done

########################################################################
######## Restart all configured services if they are available.
for service in $SERVICE_LIST_RESTART; do
  systemctl list-units --full -all | grep -Fq "$service.service" && echo "Restarting $service .." && sudo service "$service" restart
done
