#!/bin/bash

########################################################################
############# Script to install the latest Gitea version ###############
########################################################################


########################################################################
######## This script creates a backup of the running service
######## shuts it down and installs the newest available version.
########
######## Recommended alias:
######## alias update_gitea='/opt/Scripts/update_gitea.sh'

########################################################################
######## Get the latest release via Github api.
raw_release=$(curl -s https://api.github.com/repos/go-gitea/gitea/releases/latest | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | cut -c 2-)
echo "Gitea v$raw_release will be installed"

########################################################################
######## Backup the running configuration.
sudo su - gitea -c "rm ~/gitea-dump-*.zip"
sudo su - gitea -c "~/bin/gitea dump -c /srv/Konfiguration/Gitea/app.ini --skip-log --skip-custom-dir --skip-lfs-data --skip-attachment-data"

########################################################################
######## Stop the running service.
sudo service gitea stop
if ( systemctl is-active --quiet gitea  ); then
  echo "Failed to stop the service, terminating..."
  exit
fi

########################################################################
######## Download the latest files to a temporary location.
mkdir /tmp/gitea 2>/dev/null
wget --inet4-only --show-progress -qO "/tmp/gitea/gitea" "https://dl.gitea.io/gitea/$raw_release/gitea-$raw_release-linux-amd64"
wget --inet4-only --show-progress -qO "/tmp/gitea/gitea.asc" "https://dl.gitea.io/gitea/$raw_release/gitea-$raw_release-linux-amd64.asc"

########################################################################
######## Verify the download and install the new version.
if ( gpg --verify /tmp/gitea/gitea.asc /tmp/gitea/gitea 2>/dev/null ); then
  # Exchange binary
  sudo chmod 500 /tmp/gitea/gitea
  sudo chown gitea:gitea /tmp/gitea/gitea
  sudo rm -rf /opt/gitea/bin/gitea.old
  sudo mv /opt/gitea/bin/gitea /opt/gitea/bin/gitea.old
  sudo mv /tmp/gitea/gitea /opt/gitea/bin/gitea
  echo "Installation finished"
else
  echo "Download is invalid, terminating..."
fi

########################################################################
######## Clean up all temporary data.
sudo rm -rf /tmp/gitea

########################################################################
######## Start the service.
sudo service gitea start
